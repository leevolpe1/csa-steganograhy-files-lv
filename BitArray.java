public class BitArray {
    public static void main(String[] args) {
        int[] bitArray = {0b11011010, 0b10101010}; // Example array, adjust as needed
        for (int i = 0; i < bitArray.length; i++) {
            bitArray[i] &= 0b11110000; // Using bitwise AND with a mask to set last 4 digits to zero
        }
        for (int i = 0; i < bitArray.length; i++) {
            System.out.println(Integer.toBinaryString(bitArray[i]));
        }
    }
    public static String interchangeDigits(int binaryArray, int binaryArray2) {
        String binaryString = String.valueOf(binaryArray);
        char[] binaryChar = binaryString.toCharArray();
        String binaryString2 = String.valueOf(binaryArray2);
        char[] binaryChar2 = binaryString2.toCharArray();
        if (binaryChar.length >= 8 && binaryChar2.length >= 8) {
            char temp = binaryChar[2];
            binaryChar[2] = binaryChar[0];
            binaryChar[0] = temp;
        } else {
            System.out.println("Binary string should have at least 8 digits.");
        }
        if (binaryChar.length >= 8 && binaryChar2.length >= 8) {
            binaryChar2[5] = binaryChar[0];
            binaryChar2[6] = binaryChar[1];
            binaryChar2[7] = binaryChar[2];
        }
        return String.valueOf(binaryChar2);
    }
}

